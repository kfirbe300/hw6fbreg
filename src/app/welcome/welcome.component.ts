import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { ActivatedRoute } from "@angular/router";

import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { Router } from '@angular/router';



@Component({
  selector: 'welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  name = '';
  user;
  isLogged = false;

  constructor(public authService: AuthService, private route: ActivatedRoute,private router:Router, public afAuth: AngularFireAuth ) 
  {
    this.afAuth.user.subscribe(userInfo=>{
      if(this.authService.isAuth())
      {
        this.user = userInfo;
      }});
   }

  ngOnInit() {
    this.getCurrentUser();

    this.route.queryParams
      .subscribe(params => {
        console.log(params); 
        this.name = params.name;
        console.log(this.name); 
      });
 
  }

  getCurrentUser()
  {
    this.authService.isAuth().subscribe( auth =>
    {
      if(auth)
      {
        console.log('user logged');
        this.isLogged = true;
      }
      else
      {
        console.log('user isnt logged'); 
        this.isLogged = false;
      }
    })
  }

  onLogout()
  {
    console.log("logout");
    this.afAuth.auth.signOut();
    this.router.navigate(['/signup']);
  }

}